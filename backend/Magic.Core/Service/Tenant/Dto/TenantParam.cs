﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magic.Core.Service;

public class QueryTenantPageInput:PageParamBase
{
    /// <summary>
    /// 公司名称
    /// </summary>
    public string Name { get; set; }

    public string Host { get; set; }
}

public class AddTenantInput
{
    /// <summary>
    /// 公司名称
    /// </summary>
    [Required(ErrorMessage = "公司名称")]
    public  string Name { get; set; }

    /// <summary>
    /// 管理员名称
    /// </summary>
    [Required(ErrorMessage = "管理员名称")]
    public  string AdminName { get; set; }

    /// <summary>
    /// 主机名称
    /// </summary>
    public  string Host { get; set; }

    /// <summary>
    /// 数据库连接
    /// </summary>
    public string Phone { get; set; }

    /// <summary>
    /// 电子邮箱
    /// </summary>
    [Required(ErrorMessage = "电子邮箱")]
    public  string Email { get; set; }
}

public class EditTenantInput : AddTenantInput { 
    public long Id { get; set; }
}

public class GrantTenantMenuInput
{
    /// <summary>
    /// 租户id
    /// </summary>
    public long Id { get; set; }
    public List<long> GrantMenuIdList { get; set; }
}
