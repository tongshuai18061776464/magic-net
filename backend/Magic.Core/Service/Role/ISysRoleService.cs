﻿using Furion.DependencyInjection;
using Magic.Core.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Core.Service;

public interface ISysRoleService : ITransient
{
    Task Add(AddRoleInput input);
    Task Delete(PrimaryKeyParam input);
    Task<string> GetNameByRoleId(long roleId);
    Task<dynamic> GetRoleDropDown();
    Task<SysRole> Get(PrimaryKeyParam input);
    Task<dynamic> List(QueryRolePageInput input);
    Task<List<long>> GetUserDataScopeIdList(List<long> roleIdList, long orgId);
    Task<List<RoleOutput>> GetUserRoleList(long userId);
    Task GrantData(GrantRoleInput input);
    Task GrantMenu(GrantRoleInput input);
    Task<List<long>> OwnData(PrimaryKeyParam input);
    Task<List<OwnMenuOutput>> OwnMenu(OwnMenuInput input);
    Task<PageList<SysRole>> PageList(QueryRolePageInput input);
    Task Update(EditRoleInput input);
}
