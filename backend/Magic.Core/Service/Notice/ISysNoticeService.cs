﻿using Furion.DependencyInjection;
using Magic.Core.Entity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Magic.Core.Service;

public interface ISysNoticeService : ITransient
{
    Task Add(AddNoticeInput input);
    Task ChangeStatus(ChangeStatusNoticeInput input);
    Task Delete(Core.PrimaryKeyParam input);
    Task<NoticeDetailOutput> Get(Core.PrimaryKeyParam input);
    Task<PageList<SysNotice>> PageList(QueryNoticePageInput input);
    Task<PageList<NoticeReceiveOutput>> ReceivedNoticePageList(QueryNoticePageInput input);
    Task Update(EditNoticeInput input);

    Task<dynamic> UnReadNoticeList(QueryNoticePageInput input);
}
