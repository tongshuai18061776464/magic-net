﻿using Furion.DynamicApiController;
using Magic.Application;
using Magic.Application.Entity;
using Magic.Core;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Magic.Web.Core;

    /// <summary>
    /// @(@Model.BusName)服务
    /// </summary>
    [ApiDescriptionSettings("Application",Name = "@Model.ClassName", Order = 1,Tag="@Model.BusName")]
    public class @(@Model.ClassName)Controller : IDynamicApiController
    {
        private readonly I@(@Model.ClassName)Service _service;

        public @(@Model.ClassName)Controller(I@(@Model.ClassName)Service service)
        {
            _service = service;
        }

        /// <summary>
        /// 分页查询@(@Model.BusName)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("/@Model.ClassName/page")]
        public async Task<PageList<@(@Model.ClassName)>> Page([FromQuery] Query@(@Model.ClassName)PageInput input)
        {
           return await _service.Page(input);
        }

        /// <summary>
        /// 增加@(@Model.BusName)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("/@Model.ClassName/add")]
        public async Task Add(Add@(@Model.ClassName)Input input)
        {
            await _service.Add(input);
        }

        /// <summary>
        /// 删除@(@Model.BusName)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("/@Model.ClassName/delete")]
        public async Task Delete(PrimaryKeyParam input)
        {
            await _service.Delete(input);
        }

        /// <summary>
        /// 更新@(@Model.BusName)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("/@Model.ClassName/edit")]
        public async Task Update(Edit@(@Model.ClassName)Input input)
        {
            await _service.Update(input);
        }

        /// <summary>
        /// 获取@(@Model.BusName)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("/@Model.ClassName/detail")]
        public async Task<@(@Model.ClassName)> Get([FromQuery] PrimaryKeyParam input)
        {
            return await _service.Get(input);       
        }

        /// <summary>
        /// 获取@(@Model.BusName)列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("/@Model.ClassName/list")]
        public async Task<List<@(@Model.ClassName)>> List([FromQuery] Query@(@Model.ClassName)PageInput input)
        {
            return await _service.List(input);       
        }
    }

